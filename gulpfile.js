const requireDir = require('require-dir');

global.gulpConfig = require('./gulp/config'); // eslint-disable-line global-require

// Require all tasks in gulp/tasks, including subfolders
requireDir('./gulp/', { recurse: true });
