module.exports = {
  purge: [
    './src/**/*.js',
  ],
  theme: {
    screens: {
      ns: '30rem',
      lg: '60rem',
    },
    colors: {
      transparent: 'transparent',
      primary: 'var(--color-primary)',
      secondary: 'var(--color-secondary)',
      tertiary: 'var(--color-tertiary)',
      alpha: 'var(--color-alpha)',
      beta: 'var(--color-beta)',
      kappa: 'var(--color-kappa)',
      lambda: 'var(--color-lambda)',
      psi: 'var(--color-psi)',
      omega: 'var(--color-omega)',
      success: 'var(--color-success)',
      warning: 'var(--color-warning)',
      danger: 'var(--color-danger)',
    },
    borderColor: theme => ({
      ...theme('colors'),
      default: theme('colors.kappa', 'currentColor'),
    }),
  },
  variants: {
    display: ['responsive', 'group-hover'],
    backgroundColor: ['responsive', 'hover', 'focus', 'group-hover'],
    textColor: ['responsive', 'hover', 'focus', 'group-hover'],
  }
}
