const gulp = require('gulp');
const debug = require('gulp-debug');
const tap = require('gulp-tap');
const fs = require('fs');

const iconNames = [];

function normalizeNames(icon) {
  return icon.replace('.svg', '')
    .replace(/-(\w)/g, (match) => match.toUpperCase().replace('-', ''))
    .replace(/.*\//, '');
}

gulp.task('svg', () =>
  gulp.src(['./src/icons/**/*.svg'])
    .pipe(debug({ title: 'svg to icon', showFiles: false }))
    .pipe(tap((file) => {
      iconNames.push(file.relative);
    })).on('end', () => {
      if (!iconNames.length) return null;
      const lines = iconNames.sort().map(icon => icon.replace('\\', '/')).map(icon => `export { ReactComponent as ${normalizeNames(icon)} } from './${icon}';`);
      const string = lines.join('\n');
      fs.writeFileSync(`./src/icons/index.js`, string);
    })
);
