import React, { useState, useEffect } from 'react'
import Icon from 'components/Icon'
import VoiceList from 'components/VoiceList'
import voicesSrc from 'voices.json'

const tags = Object.keys(voicesSrc.reduce((acc, curr) => {
  curr.tags.forEach(tag => acc[tag] = true)
  return acc
}, {}))

const orders = [
  { id: 'ascending', name: 'Ascending', compare: (a, b) => a.name.localeCompare(b.name) },
  { id: 'descending', name: 'Descending', compare: (a, b) => b.name.localeCompare(a.name) },
]
const getOrder = order => orders.find(({ id }) => id === order)

const classes = {
  body: 'alpha bg-omega mx-3',
  header: 'max-w-4xl mx-auto my-4 flex flex-wrap',
  search: 'relative flex bg-psi rounded-full my-1 order-1',
  searchInput: 'text-alpha bg-transparent flex-auto self-center focus:outline-none',
  searchIcon: 'self-center mx-1',
  searchCloseIcon: 'self-center mx-3',
  separation: 'flex-auto order-2',
  select: 'flex mx-2 lg:mx-5 my-1 order-last',
  selectInput: 'text-beta bg-psi ml-3',
  random: 'order-3 lg:order-last',
}

export default () => {
  const [search, setSearch] = useState('')
  const [tag, setTag] = useState('')
  const [order, setOrder] = useState(getOrder('ascending'))
  const [voices, setVoices] = useState(voicesSrc)
  const [favourites, setFavourites] = useState([])
  const [active, setActive] = useState('')

  useEffect(() => {
    const pattern = search.toLocaleLowerCase()
    setVoices(
      voicesSrc
        .filter(voice => voice.name.toLocaleLowerCase().includes(pattern))
        .filter(voice => !tag || voice.tags.includes(tag))
        .sort(order.compare)
    )
  }, [search, tag, order])

  return (
    <div className={classes.body}>
      <header className={classes.header}>
        <div className={classes.search}>
          <Icon name="search" className={classes.searchIcon} />
          <input
            className={classes.searchInput}
            value={search}
            onChange={e => setSearch(e.target.value)}
            placeholder="Type something"
          />
          <button
            className={classes.searchCloseIcon}
            type="button"
            onClick={() => setSearch('')}
          >
            <Icon name="searchClose" />
          </button>
        </div>
        <div className={classes.separation} />
        <div className={classes.select}>
          <Icon name="filter" />
          <select
            className={classes.selectInput}
            value={tag}
            onChange={e => setTag(e.target.value)}
          >
            <option value="">All</option>
            {tags.map(tag =>
              <option key={tag} value={tag}>{tag}</option>
            )}
          </select>
        </div>
        <div className={classes.select}>
          <Icon name="order" />
          <select
            className={classes.selectInput}
            value={order.id}
            onChange={e => setOrder(getOrder(e.target.value))}
          >
            {orders.map(order =>
              <option key={order.id} value={order.id}>{order.name}</option>
            )}
          </select>
        </div>

        <button
          className={classes.random}
          type="button"
          onClick={() => {
            const random = Math.floor(Math.random() * voices.length)
            setActive(voices[random].id)
          }}
        >
          <Icon name="buttonRandom" />
        </button>
      </header>
      <VoiceList {...{
        name: 'Favourite voices',
        voices: favourites,
        active,
        setActive,
        favourites,
        setFavourites,
      }} />
      <VoiceList {...{
        name: 'Pro voices',
        voices,
        active,
        setActive,
        favourites,
        setFavourites,
      }} />
    </div>
  );
}
