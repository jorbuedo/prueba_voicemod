export const access = (obj, path) => path.reduce((o, i) => (o && o[i] !== 'undefined') ? o[i] : undefined, obj);

export const topDomain = window.top === window.self? document.domain : document.referrer.match(/^https?:\/\/([^:/?]*)/i)[1]

export const isDevEnvironment = process.env.REACT_APP_NODE_ENV === 'development' || process.env.NODE_ENV !== 'production'

export const camelize = (string) => {
  return string.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
    return index === 0 ? word.toLowerCase() : word.toUpperCase();
  }).replace(/-|_/g, '');
}
