import React from 'react'
import { render, wait } from '@testing-library/react'
import userEvent from "@testing-library/user-event"


import App from './App'

it('filters are empty and list is full', async () => {
  const { container } = render(<App />)

  expect(container).toHaveTextContent('8bits')
  expect(container).toHaveTextContent('Alien')
})

it('search string reduces the list', async () => {
  const { container, getByPlaceholderText } = render(<App />)

  const search = getByPlaceholderText('Type something')
  await userEvent.type(search, "alien")

  await wait(() => {
    expect(container).not.toHaveTextContent('8bits')
    expect(container).toHaveTextContent('Alien')
  })
  
})
