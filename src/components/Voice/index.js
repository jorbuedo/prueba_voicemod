import React from 'react'
import Icon from 'components/Icon'

const classes = {
  wrapper: 'relative mx-auto group',
  select: 'focus:outline-none',
  icon: 'rounded-full bg-beta group-hover:bg-alpha',
  text: 'text-sm',
  textSelected: 'text-primary',
  textUnselected: 'text-beta group-hover:text-alpha',
  favourite: 'absolute top-0 right-0 w-8 h-8 rounded-full bg-alpha hidden group-hover:block focus:outline-none',
  favouriteIcon: 'm-auto',
  isFavourite: 'bg-secondary',
}
export default ({ voice, active, setActive, favourites, setFavourites }) => {
  const isFavourite = favourites.find(favourite => voice.id === favourite.id)
  const isSelected = active === voice.id
  return (
    <div
      className={classes.wrapper}
    >
      <button
        type="button"
        className={classes.select}
        onClick={() => setActive(voice.id)}
      >
        <img
          className={classes.icon}
          style={isSelected? { background: 'linear-gradient(217deg, var(--color-primary), var(--color-secondary))' } : {}}
          src={`${process.env.PUBLIC_URL}/images/${voice.icon}`}
          alt=""
        />
        <span
          className={`${classes.text} ${isSelected ? classes.textSelected : classes.textUnselected}`}
        >
          {voice.name}
        </span>
      </button>
      <button
        className={classes.favourite}
        onClick={() => {
          if (isFavourite) {
            setFavourites(favourites.filter(favourite => voice.id !== favourite.id))
          } else {
            setFavourites([voice, ...favourites])
          }
        }}
      >
        <Icon className={classes.favouriteIcon} name={`voiceFavourite${isFavourite? '' : 'Off'}`}/>
      </button>
    </div>
  )
}
