import React from 'react'
import Voice from 'components/Voice'

const classes = {
  wrapper: 'max-w-4xl m-auto lg:px-12',
  header: 'my-8 flex',
  title: 'text-kappa uppercase',
  line: 'flex-auto self-center ml-4 border-lambda border-t-2',
  content: 'grid grid-flow-row grid-cols-3 lg:grid-cols-6 gap-4 lg:gap-8',
}

export default ({ className = '', name, voices, active, setActive, favourites, setFavourites }) =>
  <section className={`${classes.wrapper} ${className}`}>
    <header className={classes.header}>
      <h2 className={classes.title}>{name}</h2>
      <hr className={classes.line}/>
    </header>
    <div className={classes.content}>
      {voices.map(voice =>
        <Voice key={voice.id} {...{ voice, active, setActive, favourites, setFavourites }} />
      )}
    </div>
  </section>
