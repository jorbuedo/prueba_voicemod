import React from 'react';
import * as icons from 'icons';
import { camelize } from 'utils';

export default function Icon({ name,  ...props}) {
  const Icon = icons[camelize(name)];
  if (!Icon) return null;
  return (<Icon className="w1" {...props} />);
}
